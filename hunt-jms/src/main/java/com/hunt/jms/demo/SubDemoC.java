package com.hunt.jms.demo;

import org.apache.activemq.command.ActiveMQTextMessage;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author ouyangan
 * @Date 2017/1/17/16:11
 * @Description
 */
public class SubDemoC implements MessageListener {
    //计数器
    private AtomicInteger count = new AtomicInteger(1);

    @Override
    public void onMessage(Message message) {
        try {
            System.out.println();
            Thread.currentThread().sleep(2000L);
            ActiveMQTextMessage message1 = (ActiveMQTextMessage) message;
            System.out.println("C接收消息-> " +message1.getText());
        } catch (InterruptedException | JMSException e) {
            e.printStackTrace();
        }
    }
}
